package strthrottle

import (
	"context"
	"fmt"
	"sync"
	"time"
)

type Throttle struct {
	trackedItemsMutex sync.Mutex
	trackedItems      map[string]time.Time

	trackedItemsLimit int

	coolDown time.Duration
	now      func() time.Time

	// hooks
	onItemPassedThrough    func(itemString string)
	onItemNotPassedThrough func(itemString string)
	onOldItemRemoved       func(itemString string)
	onOldItemsCleanupDone  func(currentlyTrackedItemsCount int)
}

func New(coolDown time.Duration, now func() time.Time) *Throttle {
	return &Throttle{
		trackedItemsMutex: sync.Mutex{},
		trackedItems:      make(map[string]time.Time),
		trackedItemsLimit: 1000,
		coolDown:          coolDown,
		now:               now,
	}
}

func (t *Throttle) OnItemPassedThrough(fn func(string)) {
	t.onItemPassedThrough = fn
}

func (t *Throttle) OnItemNotPassedThrough(fn func(string)) {
	t.onItemNotPassedThrough = fn
}

func (t *Throttle) OnOldItemRemoved(fn func(string)) {
	t.onOldItemRemoved = fn
}

func (t *Throttle) OnOldItemsCleanupDone(fn func(int)) {
	t.onOldItemsCleanupDone = fn
}

func (t *Throttle) RemoveOldItemsPeriodically(interval time.Duration) context.CancelFunc {
	ticker := time.NewTicker(interval)

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				t.removeOldItems()

				if t.onOldItemsCleanupDone != nil {
					t.onOldItemsCleanupDone(len(t.trackedItems))
				}

				ticker.Reset(interval)
			}
		}
	}()

	return cancel
}

func (t *Throttle) LimitTrackedItems(limit int) {
	if limit <= 0 {
		panic("tracked items limit must be positive")
	}

	t.trackedItemsLimit = limit
}

func (t *Throttle) GoThrough(item fmt.Stringer) bool {
	t.trackedItemsMutex.Lock()
	defer t.trackedItemsMutex.Unlock()

	itemString := item.String()

	lastSeen, ok := t.trackedItems[itemString]
	if !ok {
		t.trackItem(itemString)

		if t.onItemPassedThrough != nil {
			t.onItemPassedThrough(itemString)
		}

		return true
	}

	if t.isItemOld(lastSeen) {
		t.trackItem(itemString)

		if t.onItemPassedThrough != nil {
			t.onItemPassedThrough(itemString)
		}

		return true
	}

	if t.onItemNotPassedThrough != nil {
		t.onItemNotPassedThrough(itemString)
	}

	return false
}

func (t *Throttle) trackItem(itemString string) {
	t.trackedItems[itemString] = t.now()
}

func (t *Throttle) isItemOld(lastSeen time.Time) bool {
	return lastSeen.Add(t.coolDown).Before(t.now())
}

func (t *Throttle) removeOldItems() {
	t.trackedItemsMutex.Lock()
	defer t.trackedItemsMutex.Unlock()

	for itemString, lastSeen := range t.trackedItems {
		if t.isItemOld(lastSeen) {
			delete(t.trackedItems, itemString)

			if t.onOldItemRemoved != nil {
				t.onOldItemRemoved(itemString)
			}
		}
	}
}

package strthrottle_test

import (
	"strthrottle"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type testStringer struct {
	value string
}

func (s *testStringer) String() string {
	return s.value
}

func mustParseTime(timeString string) time.Time {
	t, err := time.Parse(time.RFC3339, timeString)
	if err != nil {
		panic(err)
	}

	return t
}

func TestThrottle_GoThrough(t *testing.T) {
	t.Run("The item passes for the first time and it is the first tracked item", func(t *testing.T) {
		// Given
		th := strthrottle.New(time.Second, func() time.Time {
			return mustParseTime("2007-01-01T12:00:00Z")
		})

		// When
		ok := th.GoThrough(&testStringer{value: "a"})

		// Then
		assert.True(t, ok)
	})

	t.Run("The item passes for the first time", func(t *testing.T) {
		// Given
		th := strthrottle.New(time.Second, func() time.Time {
			return mustParseTime("2007-01-01T12:00:00Z")
		})

		ok := th.GoThrough(&testStringer{value: "a"})
		require.True(t, ok)

		// When
		ok = th.GoThrough(&testStringer{value: "b"})

		// Then
		assert.True(t, ok)
	})

	t.Run("The item passes for the second time and the throttle has not cooled down yet", func(t *testing.T) {
		// Given
		now := mustParseTime("2007-01-01T12:00:00Z")

		th := strthrottle.New(5*time.Second, func() time.Time {
			return now
		})

		ok := th.GoThrough(&testStringer{value: "a"})
		require.True(t, ok)

		// When
		now = now.Add(4 * time.Second)

		ok = th.GoThrough(&testStringer{value: "a"})

		// Then
		assert.False(t, ok)
	})

	t.Run("The item passes for the second time and the throttle has already cooled down", func(t *testing.T) {
		// Given
		now := mustParseTime("2007-01-01T12:00:00Z")

		th := strthrottle.New(5*time.Second, func() time.Time {
			return now
		})

		ok := th.GoThrough(&testStringer{value: "a"})
		require.True(t, ok)

		// When
		now = now.Add(6 * time.Second)

		ok = th.GoThrough(&testStringer{value: "a"})

		// Then
		assert.True(t, ok)
	})
}

func TestThrottle_RemoveOldItemsPeriodically(t *testing.T) {
	// Given
	now := mustParseTime("2007-01-01T12:00:00Z")

	th := strthrottle.New(time.Second, func() time.Time {
		return now
	})

	ok := th.GoThrough(&testStringer{value: "a"})
	require.True(t, ok)

	ok = th.GoThrough(&testStringer{value: "b"})
	require.True(t, ok)

	ok = th.GoThrough(&testStringer{value: "c"})
	require.True(t, ok)

	quit := make(chan struct{})

	currentlyTrackedItems := 1

	th.OnOldItemsCleanupDone(func(i int) {
		currentlyTrackedItems = i

		quit <- struct{}{}
	})

	// When
	now = now.Add(2 * time.Second)

	cancel := th.RemoveOldItemsPeriodically(time.Millisecond)
	defer cancel()

	<-quit

	// Then
	assert.Equal(t, 0, currentlyTrackedItems)
}
